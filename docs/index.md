
## 博客mkdocs

### 仓库

[gitlab]( https://gitlab.com/xuyq123/myblog-mkdocs ) &ensp; [gitee]( https://gitee.com/xy180/myblog-mkdocs ) &ensp; [github]( https://github.com/scott180/myblog-mkdocs ) &ensp; [gitlab_mkdocs]( https://xuyq123.gitlab.io/myblog-mkdocs/ )  &ensp; [github_mkdocs]( https://scott180.github.io/myblog-mkdocs/ ) 
	
### 文档

```
site_name: 博客mkdocs

theme:
  name: mkdocs
  
nav: 
  - 笔记: 
    - note: 笔记/note.md
    - java: 笔记/java.md
    - gitNote: 笔记/gitNote.md
    - linuxNote: 笔记/linuxNote-x.md
  - 数据库:
    - mysqlNote: 数据库/mysqlNote.md
    - oracleNote: 数据库/oracleNote.md
    - redis: 数据库/redis.md
    - mongo: 数据库/mongo.md
    - 数据库隔离级别: 数据库/数据库隔离级别.md
  - 资料:
    - eclipse: 资料/eclipse.md
    - docker: 资料/docker.md
    - mysql开启log-bin日志: 资料/mysql开启log-bin日志.md
    - gitlab、github、gitee布署mkdocs主题仓库: 资料/gitlab、github、gitee布署mkdocs主题仓库.md
  - 文档:
    - 古文诗词: 文档/古文诗词.md
    - 书法练习轨迹ReadMe: 文档/书法练习轨迹ReadMe.md

```

---
	
> `颜真卿-多宝塔碑` <br/>
![颜真卿-多宝塔碑]( https://xyqin.coding.net/p/my/d/imgs/git/raw/master/%E4%B9%A6%E6%B3%95%E5%AD%97%E5%B8%96/颜真卿-多宝塔碑.jpg)

