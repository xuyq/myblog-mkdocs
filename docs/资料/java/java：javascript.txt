	
	
	
	
	
	//函数两种创建方式
		var F1=function(){}
		function F2(){
			alert(1);
		}
		var f3=new F2();
		alert(typeof(F1));//函数名 funciton
		alert(typeof(F2));//函数名 function
		alert(typeof(f3));//函数引用object  
		F2();//函数名加括号表示调用函数
		
匿名函数		
	
	//闭包 ()()
	(function(i){
		alert(i);
	})(11);

	//匿名函数
	var f=function(i){
		alert(i);
	};
	f(22);//调用函数
	alert(typeof(f));//f是function

	//有参函数
	function F(i){
		alert(i);
	}
	var f2=new F(33);//函数名（）表示调用函数
	alert(typeof(f2));//f2是function
	F(55);//函数名（）表示调用函数
	
	
	
	
	
	
	//例1 函数声明
function F1(){
	alert("function declaration 1");
}

//例2 函数表达式。函数是new出来的立刻执行，相当于先动态创建了函数再将其调用 。如例3
var f2=new function(){
	//alert("new function expression 2");
};

//例3
function Fun3(){//声明函数
	alert("function Fun3");
}
var f31=new Fun3();//Fun3()函数名+括号 可以直接调用函数
//alert("typeof(f31):  "+typeof(f31));//Fun3的引用赋值给f31。f31 是object
//alert("f31 "+f31);
f31;

var f32=Fun3;//将Fun3赋值给f32。f32是function
alert("typeof(f32)--"+typeof(f32));//function
f32();//函数名+括号 可以直接调用函数
alert(f32);

//例4 函数表达式
var f4=function(){
	alert("function expression 4");
};


//alert("dec  "+typeof(F1));//funciton
//alert("exp  "+typeof(f2));//function 是new 出来的，f2是object
//alert("exp  "+typeof(f4));//function




//声明函数
function F1(){
	alert("F1");
	
}

//函数表达式
var f2=function(){
	alert("F2");
};

//调用函数
//F1();
//f();

//声明函数  需要在function加括号转换为表达式在其后再加上括号才能直接调用函数
(function F3(){
	alert("F3");
	
})();

//将函数声明转换为函数表达式再加括号可以直接调用函数
!function F31(){
	alert("!!");
	
}();

+function F32(){
	alert("++");
	
}();

-function F33(){
	alert("--");
	
}();
//函数表达式 后面加括号直接调用函数
var f3=function(){
	alert("F4");
}();



函数调用的三种方式:
obj.myFunc();
myFunc.call(obj,arg);
myFunc.apply(obj,[arg1,arg2..]); 

call和apply可以用来重新定义函数的执行环境，也就是this的指向。通过一个操作DOM的例子来理解。

function changeStyle(attr, value){
    this.style[attr] = value;
}
var box = document.getElementById('box');
window.changeStyle.call(box, "height", "200px");

call中的第一个参数用于指定将要调用此函数的对象，在这里，changeStyle函数将被box对象调用，this指向了box对象，如果不用call的话，程序报错，因为window对象中没有style属性。
apply的用法:

window.changeStyle.apply(box, ['height', '200px']);


call用来调用另一个对象的方法，但调用的对象是可以作为参数传入的。
function A(name){//A 类
    this.name=name;
    this.showName=function(){
        alert(this.name);
    }
}
 
function B(name,age){ //B 类
    this.name=name;
    this.age=age;
}
var a=new A("aaa"); //a对象是有showName方法的
var b=new B('bbb',10);//b对象没有showName方法
a.showName.call(b);//但是这里showName，显示的是b的名字，也就是b调用了a的showName方法

call最经典的应用就是继承。javascript是没有对象继承概念的，我们只能用一些方法实现。
function A(name){//A 类 还是这个例子，A有showName
    this.name=name;
    this.showName=function(){
        alert(this.name);
    }
}
 
function B(name,age){ //B 类，
 
    //B类中通过A.call，相当于执行了A(name),但在A中所有this其实都是B。那么B就有name这个属性，同时也拥有showName方法了。
    A.call(this,name);
    this.age=age;
}
var b=new B('bbb',10);
b.showName();//B继承了A，所以b也有showName方法。