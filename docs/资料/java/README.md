## MyNotes
> 个人笔记
java  linux  javascript  jquery  mysql oracle
  
*   [1、笔记](#note)
    *   [1.1、linux](#linuxNote)
    *   [1.1、mongo](#mongoNote)
    *   [1.3、docker](#dockerNote)
    *   [1.4、javascript](#javascriptNote)
    *   [1.5、git](#gitNote)
*   [2、常用软件](#commonSoft)
    *   [2.1、jdk](#jdkSoft)
    *   [2.2、tomcat](#tomcatSoft)
    *   [2.3、eclipse](#eclipse)
    
*****************************************************************

> [马克飞象]( https://maxiang.io )      [作业部落]( https://www.zybuluo.com/mdeditor )    [markdown]( https://jbt.github.io/markdown-editor/ )


## 1、笔记 <h2 id="note"></h2>

###  1.1、linux <h3 id="linuxNote"></h3>
> linux笔记 
https://github.com/scott180/MyNotes/blob/master/linux%E7%AC%94%E8%AE%B0-x.md

###  1.2、mongo <h3 id="mongoNote"></h3>
> mongo笔记 
https://github.com/scott180/MyNotes/blob/master/mongo.md

###  1.3、docker <h3 id="dockerNote"></h3>
> docker笔记 
https://github.com/scott180/MyNotes/blob/master/docker.md

###  1.4、javascriptNote <h3 id="javascriptNote"></h3>
> 深入理解javascript原型和闭包
https://github.com/scott180/MyNotes/tree/master/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3javascript%E5%8E%9F%E5%9E%8B%E5%92%8C%E9%97%AD%E5%8C%85

###  1.5、git <h3 id="gitNote"></h3>
> git笔记 
https://github.com/scott180/MyNotes/blob/master/git%E7%AC%94%E8%AE%B0.md
	
	
## 2、常用软件 <h2 id="commonSoft"></h2>
<h3 id="jdkSoft"></h3>

### 2.1、jdk 

- [ ] jdk6--jdk10都是来自官网（需要注册）
http://www.oracle.com/technetwork/java/archive-139210.html
这些jdk软件有三种格式：tar.gz、zip、bin
- 1、zip是windoxs软件，解压后会有exe格式的jdk软件，直接安装即可。
- 2、tar.gz是linux软件，需要用tar -zxvf xx.tar.gz解压。
- 3、bin也是linux软件，需解压：
   - 添加执行权限 
    chmod u+x jdk-6u45-linux-x64.bin
   - 解压 
    ./jdk-6u45-linux-x64.bin
    
- [ ] 软件名称里含有x64则是64位软件，32位为则没有。如下：
- jdk-8u162-windows-x64.zip  jdk-8u162-linux-x64.tar.gz  是64位软件
- jdk-8u72-windows-i586.zip jdk-8u72-linux-i586.tar.gz   是32位软件

>  jdk6 链接: https://pan.baidu.com/s/1z3p1DecyBVugP7cECIupyg 密码: 829h

>  jdk7 链接: https://pan.baidu.com/s/17ik9x-g3RkYEu6vah9CZVw 密码: muvr

>  jdk8 链接: https://pan.baidu.com/s/1MT8zldLnH9PuZsVR77DEAw 密码: mv5i

>  jdk9 链接: https://pan.baidu.com/s/1SMGJqedJKR3hULrpWn4eLA 密码: hai6

>  jdk10 链接: https://pan.baidu.com/s/1SHA7XNoPxBdOkaed3cunow 密码: hgyn

<h3 id="tomcatSoft"></h3>

### 2.2、tomcat
> tomcat6-tomcat9 
官网 https://archive.apache.org/dist/tomcat/

>云盘 https://pan.baidu.com/s/1yPhAfIcACTGkpIOYlEds1g 密码: j9ug


<h3 id="eclipse"></h3>

### 2.3、eclipse
> eclipse 下载及版本说明
https://github.com/scott180/MyNotes/blob/master/eclipse%20%E4%B8%8B%E8%BD%BD%E5%8F%8A%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E.md
